<?xml version="1.0"?>
<html xmlns="http://www.w3.org/1999/xhtml">

  <head>
    <title>Output-Sensitive Adaptive Metropolis Hastings for Probabilistic Programs</title>
    <!-- metadata -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="generator" content="PSS" />
    <meta name="version" content="PSS 0.0" />
    <meta name="author" content="David Tolpin" />
    <meta name="company" content="Offtopia" />
    <!-- style links -->
    <link rel="stylesheet" href="style/poster.css" type="text/css" id="posterStyle"/>
    <link rel="stylesheet" href="style/view.css" type="text/css" media="projection" id="viewStyle" />
    <link rel="stylesheet" href="style/print.css" type="text/css" media="print" id="printStyle" />
    <link rel="stylesheet" href="style/read.css" type="text/css" media="screen" id="readStyle" />

    <!-- script links -->
    <script src="./mathjax/MathJax.js" type="text/javascript">
      MathJax.Hub.Config({
      extensions: ["tex2jax.js"],
      jax: ["input/TeX","output/HTML-CSS"],
      tex2jax: {inlineMath: [["$","$"],["\\(","\\)"]]},
      noImageFonts: true
      }); 
    </script>

    <link rel="stylesheet" href="./highlight/styles/default.css">
    <script src="./highlight/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

    <script src="script/poster.js" type="text/javascript"></script>
  </head>
  <body>
      <div class="author"><b>David Tolpin, Jan Willem van de Meent,<br/>Brooks Paige, Frank Wood</b><br/>University of Oxford<br/>Oxford, UK</div>
      <h1>Output-Sensitive Adaptive Metropolis-Hastings<br/>for Probabilistic Programs</h1>
  <br style="line-height: 0; clear: both"/>

    <div class="tosafot">
        <h2><span>Preliminaries</span></h2>
      <div class="zoomable region">
        <h3><span>Probabilistic Program</span></h3>
        <ul>
        <li>&#x2022;&#x2007; A program with random computations.</li>
        <li>&#x2022;&#x2007; Distributions are conditioned by `observations'.</li>
        <li>&#x2022;&#x2007; Values of certain expressions are `predicted' &#x2014; <strong>the output</strong>.</li>
        </ul>
        <pre><code class="clojure">(let [;; Model
        dist (sample (categorical [[normal 1/2] [gamma 1/2]]))
        a (sample (gamma 1 1))
        b (sample (gamma 1 1))
        d (dist a b)]
    ;; Observations
    (observe d 1) (observe d 2)
    (observe d 4) (observe d 7)
    ;; Explanation
    (predict :d (type d))
    (predict :a a) (predict :b b)))</code></pre>

      </div>
      <div class="zoomable region">
          <h3><span>Inference Objectives</span></h3>
          <ul>
              <li>&#x2022;&#x2007; Suggest <strong>most probable explanation</strong> (MPE) - most likely assignment</li>
  for all non-evidence variable given evidence.
  <li>&#x2022;&#x2007; Approximately <strong>compute integral</strong> of the</li>
  form $\Phi=\int_{-\infty}^{\infty} \varphi(x)p(x) dx$
  <li>&#x2022;&#x2007; Continuously and <strong>infinitely generate a sequence of samples</strong>. &#x2713;</li>
          </ul>
      </div>
      <div class="zoomable region">
        <h3><span>Lighweight Metropolis-Hastings (LMH)</span></h3>
        <ul>
            <li>$\mathcal{P}$ &#x2014; probabilistic program.</li>
            <li>$\pmb{x}$ &#x2014; random variables.</li>
            <li>$\pmb{z}$ &#x2014; output.</li>
        </ul>
        <p class="algorithm">
    Run $\mathcal{P}$ once, remember $\pmb{x}, \pmb{z}$.
    <span><kwd>Loop</kwd>
    <span/>  Uniformly select $x_i$.
    <span/>  Propose a value for $x_i$.
    <span/>  Run $\mathcal{P}$, remember $\pmb{x'}, \pmb{z'}$.
    <span/>  Accept ($\pmb{x,z}=\pmb{x',z'}$)
    <span/>         or reject with MH probability.
    <span/>  Output $\pmb{z}$.
    <span><kwd>end loop</kwd>
        </p>
<p> Can we do better?  </p>
      </div>
      <div class="zoomable region">
        <h2><span>References</span></h2>
        <ol>
        <li> Christophe Andrieu and Johannes Thoms. A tutorial on adaptive MCMC. Statistics and Computing, 18(4):343–373, 2008.</li>
        <li> B. Paige, F. Wood, A. Doucet, and Y.W. Teh. Asynchronous anytime sequential Monte Carlo. In NIPS-2014, to appear.</li>
        <li> David Wingate, Andreas Stuhlmu ̈ller, and Noah D. Goodman. Lightweight implementations of probabilistic programming languages via transformational compilation. In Proc. of AISTATS-2011.</li>
        <li> Frank Wood, Jan Willem van de Meent, and Vikash Mansinghka. A new approach to probabilistic programming inference. In AISTATS-2014.</li>
    </ol>
      </div>
    </div>

    
    <div class="rashi">
       <div class="zoomable region halacha">
        <h2><span>Experiments</span></h2>
        <h3><span>Convergence &#x2014;Gaussian Process</span></h3>
$\begin{align*}
    f\sim&\mathcal{GP}(m,k),\\
    \mbox{where }m(x)=&ax^2+bx+c,\quad k(x,x′)=de^{−\frac {(x - x')^2} {2g}}.
\end{align*}$
          <p style="align: center">
            <object class="results" data="gp-ks.svg"></object>
            <br/>
            1000 samples <object class="results" data="gp-1000-choices.svg"></object>
            <br/>
            10,000 samples <object class="results" data="gp-10000-choices.svg"></object>
            <br/>
            100,000 samples <object class="results" data="gp-100000-choices.svg"></object>
          </p>
        </div>
      <div class="zoomable region halacha">
          <h3><span>Sample size &#x2014; Kalman Smoother</span></h3>
        <p>
        $\begin{align*}
    \vec{x}_t 
    &\sim 
    {\rm Norm}(\vec{A} \cdot \vec{x}_{t-1}, \vec{Q})
    ,
    &
    \vec{y}_t 
    &\sim
    {\rm Norm}(\vec{C} \cdot \vec{x}_{t}, \vec{R})
    .
\end{align*}$
        </p>
        <p>
$\begin{align*}
    \vec{A} 
    &= 
    \left[
        \begin{array}{cc}
            ~\cos \omega~
            & 
            ~-\sin \omega~
            \\
            ~\sin \omega~
            &
            ~\cos \omega~
        \end{array}
    \right]
    ,
    &
    \vec{Q}
    &=
    \left[
        \begin{array}{cc}
            ~q~
            & 
            0
            \\
            0
            &
            ~q~
        \end{array}
    \right]
    .
\end{align*}$
</p>

<p>
            100 16-dimensional observations,
            <br/>500 samples after 10,000 samples of burn-in.
            <object class="results" data="kalman-filter.svg"></object>
</p>
       </div>
      <div style="margin-top: 2em" class="zoomable region derecharetz">
        <h2><span>Acknowledgments</span></h2>
        This material is based on research sponsored by DARPA through the U.S.
        Air Force Research Laboratory under Cooperative Agreement number
        FA8750-14-2-0004.
        </div>
   </div>

    <div class="guf">
        <div class="zoomable region mishna">
            <h2><span>Metropolis Hastings with Adaptive Scheduling</span></h2>
            <ul>
            <li>&#x2022;&#x2007;Selects each $x_i$ with a different probability.</li>
            <li>&#x2022;&#x2007;Maintains vector of weights $\pmb{W}$ of random choices:</li>
        </ul>
        <p class="algorithm">
    <span/>Initialize $\pmb{W}^0$ to a constant.
    <span/>Run $\mathcal{P}$ once.
    <span/><kwd>for</kwd> $t = 1 \ldots \infty$
    <span/>  Select $x^t_i$ with probability $\alpha^t_i= {W_i^t} / \sum\limits_{i=1} ^{|\pmb{x}^t|} W_i^t$.
    <span/>  Propose a value for $x^t_i$.
    <span/>  Run $\mathcal{P}$, accept or reject with MH probability.
    <span/>  <kwd>if</kwd> accepted
    <span/>  Compute $\pmb{W}^{t+1}$ based on the <i>program output.</i>
    <span/>  <kwd>else</kwd>
    <span/>      $\pmb{W}^{t+1} \gets \pmb{W}^{t}$
    <span/>  <kwd>end if</kwd>
    <span/><kwd>end for</kwd>
        </p>
      </div>
      <div class="gmara zoomable region">
        <h2><span>Quantifying the Influence</span></h2>
        <ul>
            <li>&#x2022;&#x2007; Objective: faster convergence of <em>program output</em> $\pmb{z}$.</li>
            <li>&#x2022;&#x2007; Adaptation parameter: probabilities of selecting random choices for modification.</li>
            <li>&#x2022;&#x2007; Optimization target: maximize the <strong>change</strong> in the program output: $$R^t = \frac 1 {|\pmb{z}^t|}\sum_{k=1}^{|\pmb{z}^t|} \pmb{1}(z_k^t \ne z_k^{t-1}).$$</li>
        </ul>
        <p>
    $W_i$ reflects the anticipated change in $\pmb{z}$ from modifying $x_i$.
      </div>
      <div class="gmara zoomable region">
        <h2><span>Delayed Changes</span></h2>
        <p>Modifying <tt>x2</tt> affects the output ...</p>
        <pre><code class="clojure">(let [x1 (sample (normal 1 10))
              x2 (sample (normal x1 1))]
          (observe (normal x2 1) 2)
          (predict x1))</code></pre>
        <p>... but only when <tt>x1</tt> is also modified.</p>
      </div>
      <div class="pesek zoomable region">
          <h2><span>Back-propagating rewards</span></h2>
        <ul>
            <li>&#x2022;&#x2007;For each $x_i$, reward $r_i$ and count $c_i$ are kept.</li>
            <li>&#x2022;&#x2007;A <em>history</em> of modified random choices is attached to every $z_j$.</li>
        </ul>
        <p>
        When modification of $x_k$ accepted:
        </p>
    <p class="algorithm">
    <span/>Append $x_k$ to the history.
    <span/><kwd>if</kwd> $\pmb{z}^{t+1} \ne \pmb{z}^{t}$
    <span/>    $w \gets \frac 1 {|history|}$
    <span/>    <kwd>for</kwd> $x_m$ <kwd>in</kwd> history
    <span/>        $\overline r_m \gets r_m + w,\; c_m \gets c_m + w$
    <span/>    <kwd>end for</kwd>
    <span/>    Flush the history.
    <span/><kwd>else</kwd>
    <span/>    $c_k \gets c_k + 1$
    <span/><kwd>end if</kwd>
    </p>
    <p><b>Convergence:</b></p>
    <p>
    For <em>any partitioning</em> of $\pmb{x}$, Adaptive LMH selects variables from each partition with <strong>non-zero probability.</strong>
    </p>
    </div>
    </div>

    <div style="clear: both" />

    <div class="zoomable region musar">
      <h2><span>Contributions</span></h2>
      <ul>
          <li>&#x2022;&#x2007;A scheme of rewarding random samples based on <em>program output.</em></li>
          <li>&#x2022;&#x2007;An approach to propagation of sample rewards to MH proposal <em>scheduling</em> parameters.</li>
          <li>&#x2022;&#x2007;An application of this approach to LMH, where the probabilities of <em>selecting each variable</em> for modification are adjusted.</li>
      </ul>
    </div>

    <div class="zoomable region safek">
      <h2><span>Future Work</span></h2>
      <div class="region barcode">
        <a href="http://www.offtopia.net/nips-pp-ws-2014-ardb-poster/"><img width="100%" src="barcode.png" alt="http://www.offtopia.net/nips-pp-ws-2014-ardb-poster/" /></a>
      </div>
      <ul>
          <li>&#x2022;&#x2007;Extending the adaptation approach to other <strong>sampling methods</strong>.</li>
          <li>&#x2022;&#x2007;Reward scheme that takes into account the <strong>amount of difference</strong>
          between samples.</li>
          <li>&#x2022;&#x2007;Acquisition of <strong>dependencies</strong> between predicted expressions and
          random variables.</li>
      </ul>
      <br/>
    </div>

    <br style="clear: both" />
    
  </body>
</html>
