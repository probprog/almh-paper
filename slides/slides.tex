\documentclass{beamer}

\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[ruled]{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{minted}

\newcommand {\I} {\ensuremath {\mathbf{1\hspace{-5.5pt}1}}}
\renewcommand{\vec}[1]{\ensuremath{\boldsymbol{#1}}}

\title{Output-Sensitive Adaptive Metropolis-Hastings\\for Probabilistic Programs}
\author{David Tolpin, Jan Willem van de Meent,\\Brooks Paige, Frank Wood\\University of Oxford}
\date{Sep 8th, 2015}
  
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}

\begin{document}

\begin{frame}
\titlepage

\centering
Paper: \url{http://arxiv.org/abs/1501.05677}

Slides: \url{http://offtopia.net/almh-slides.pdf}
\end{frame}

\section{Probabilistic Programming}

\begin{frame}{Intuition}
    \textbf{Probabilistic program:}
\begin{itemize}
\item A program with random computations.
\item Distributions are conditioned by `observations'.
\item Values of certain expressions are `predicted' --- \textbf{the output}.
\end{itemize}

Can be written in any language (extended by \texttt{sample} and \texttt{observe}).
\end{frame}

\begin{frame}[fragile]{Example: Model Selection}
\begin{minted}[linenos, fontsize=\small]{clojure}
  (let [;; Model
        dist (sample (categorical [[normal 1/4] [gamma 1/4]
                                   [uniform-discrete 1/4]
                                   [uniform-continuous 1/4]]))
        a (sample (gamma 1 1))
        b (sample (gamma 1 1))
        d (dist a b)]

    ;; Observations
    (observe d 1)
    (observe d 2)
    (observe d 4)
    (observe d 7)

    ;; Explanation
    (predict :d (type d))
    (predict :a a)
    (predict :b b)))
\end{minted}
\end{frame}

\begin{frame}{Definition}

A \textbf{probabilistic program} is a stateful deterministic
computation $\mathcal{P}$:
\pause
\begin{itemize}
\item Initially, $\mathcal{P}$ expects no arguments.
	\pause
\item On every call, $\mathcal{P}$ returns
    \begin{itemize}
        \item a distribution $F$,
        \item a distribution and a value $(G, y)$,
        \item a value $z$, 
        \item or $\bot$.
    \end{itemize}
	\pause
\item Upon returning $F$, $\mathcal{P}$ expects $x\sim F$.
	\pause
\item Upon returning $\bot$, $\mathcal{P}$ terminates.
\end{itemize}
\pause
A program is run by calling $\mathcal{P}$ repeatedly until termination.

The probability of each \textbf{trace} is $\propto\prod_{i=1}^{\left|\pmb{x}\right|} p_{F_i}(x_i) \prod_{j=1}^{\left|\pmb{y}\right|}p_{G_j}(y_{j})$.
\end{frame}

\section{Inference}

\begin{frame}{Inference Objective}
\begin{itemize}
\item Suggest \textbf{most probable explanation} (MPE) - most likely assignment
  for all non-evidence variable given evidence.
  \pause
\item Approximately \textbf{compute integral} of the
  form $$\Phi=\int_{-\infty}^{\infty} \varphi(x)p(x) dx$$
  \pause
\item Continuously and \textbf{infinitely generate a sequence of samples} drawn
  from the distribution of the output expression --- so that someone
  else puts it in good use (vague but common). $\checkmark$
\end{itemize}
\end{frame}

\begin{frame}{Example: Inference Results}
  \begin{figure}[H]
      \includegraphics[scale=0.6]{models-results.pdf}
  \end{figure}
\end{frame}

\begin{frame}{Importance Sampling}
    \begin{algorithmic}
        \Loop
            \State Run $\mathcal{P}$, computing weight $w=\prod_{j=1}^{\left|\pmb{y}\right|}p_{G_j}(y_{j})$.
            \State output $\pmb{z}, w$.
        \EndLoop
    \end{algorithmic}
    \begin{itemize}
        \item[] 
         \item Simple --- good.
         \item Slow convergence (unless one knows the answer) --- bad.
     \end{itemize}
     \vfill
     Can we do better?
\end{frame}

\begin{frame}{Lightweight Metropolis-Hastings (LMH)}
\begin{algorithmic}
    \State {Run $\mathcal{P}$ once, remember $\pmb{x}, \pmb{z}$.}
    \Loop
         \State {Uniformly select $x_i$}.
         \State {Propose a value for $x_i$.}
         \State {Run $\mathcal{P}$, remember $\pmb{x'}, \pmb{z'}$}.
         \State {Accept ($\pmb{x,z}=\pmb{x',z'}$) or reject with MH probability}.
         \State {Output $\pmb{z}$.}
    \EndLoop
\end{algorithmic}
\vfill
Can we do better?
\end{frame}

\begin{frame}{Adaptive MCMC}
    Adaptation opportunities:
    \begin{enumerate}
        \item Random walk (instead of proposing from priors).
        \item Adapting random walk parameters.
        \item Selecting each $x_i$ with different probability. $\checkmark$
    \end{enumerate}
\end{frame}


\section{Output-sensitive Adaptive Metropolis-Hastings}

\begin{frame}{Lightweight MH with Adaptive Scheduling}
Maintains vector of weights $\pmb{W}$ of random choices:
\vspace{\baselineskip}
\begin{algorithmic}[1]
    \State Initialize $\pmb{W}^0$ to a constant.
    \State {Run $\mathcal{P}$ once.}
\For {$t = 1 \ldots \infty$}
  \State {Select $x^t_i$ with probability 
    $\alpha^t_i= {W_i^t} / \sum\limits_{i=1} ^{|\pmb{x}^t|} W_i^t$.}
  \State {Propose a value for $x^t_i$.}
  \State {Run $\mathcal{P}$, accept or reject with MH probability.}
  \If {accepted}
  \State {Compute $\pmb{W}^{t+1}$ based on the \textit{program output.}}
  \Else
      \State $\pmb{W}^{t+1} \gets \pmb{W}^{t}$
  \EndIf
\EndFor
\end{algorithmic}
\end{frame}

\begin{frame}{Quantifying Influence of Program Output}
    \begin{itemize}
        \item Objective: faster convergence of \textit{program output} $\pmb{z}$.
        \item Adaptation parameter: probabilities of selecting random choices for modification.
        \item Optimization target: maximize the \textbf{change} in the program output: \[R^t = \frac 1 {|\pmb{z}^t|}\sum_{k=1}^{|\pmb{z}^t|} \I(z_k^t \ne z_k^{t-1}).\]
    \end{itemize}
    \vfill
    $W_i$ reflects the anticipated change in $\pmb{z}$ from modifying $x_i$.
\end{frame}

\begin{frame}[fragile]{Delayed Changes}
Modifying \texttt{x2} affects the output ...
\vspace{\baselineskip}
\begin{minted}[linenos, fontsize=\small]{clojure}
    (let [x1 (sample (normal 1 10))
          x2 (sample (normal x1 1))]
      (observe (normal x2 1) 2)
      (predict x1))
\end{minted}
\vspace{\baselineskip}
... but only when \texttt{x1} is also modified.
\end{frame}

\begin{frame}{Backpropagating rewards}
    \begin{itemize}
        \item For each $x_i$, reward $r_i$ and count $c_i$ are kept.
        \item A \textit{history} of modified random choices is attached to every $z_j$.
    \end{itemize}
    \vfill
    When modification of $x_k$ accepted:
    \begin{algorithmic}[1]
        \State Append $x_k$ to the history.
        \If {$\pmb{z}^{t+1} \ne \pmb{z}^{t}$}
            \State $w \gets \frac 1 {|history|}$
            \For {$x_m$ {\bf in} history}
                \State $\overline r_m \gets r_m + w,\; c_m \gets c_m + w$ \label{alg:award-reward}
            \EndFor
            \State Flush the history.
        \Else
            \State $c_k \gets c_k + 1$
        \EndIf
    \end{algorithmic}
\end{frame}

\section{Empirical Evaluation}

\begin{frame}{Convergence --- GP hyperparameter estimation}
\begin{align*}
    f\sim&\mathcal{GP}(m,k),\\
    \mbox{where }m(x)=&ax^2+bx+c,\quad k(x,x′)=de^{−\frac {(x - x')^2} {2g}}.
\end{align*}
\begin{figure}[t]
    \begin{minipage}[c]{0.45\linewidth}
        \centering
        \includegraphics[scale=0.35]{gp-ks.pdf} \\
    \end{minipage}
    \begin{minipage}[c]{0.45\linewidth}
        \centering
        \includegraphics[scale=0.25]{gp-1000-choices.pdf} \\
        {\scriptsize 1000 samples} \\
        \includegraphics[scale=0.25]{gp-10000-choices.pdf} \\
        {\scriptsize 10000 samples} \\
        \includegraphics[scale=0.25]{gp-100000-choices.pdf} \\
        {\scriptsize 100000 samples}
    \end{minipage}
\label{fig:gp}
\end{figure}
\end{frame}


\begin{frame}{Sample Size --- Kalman Smoother}
\begin{align*}
    \vec{x}_t 
    &\sim 
    {\rm Norm}(\vec{A} \cdot \vec{x}_{t-1}, \vec{Q})
    ,
    &
    \vec{y}_t 
    &\sim
    {\rm Norm}(\vec{C} \cdot \vec{x}_{t}, \vec{R})
    .
\end{align*}
\begin{align*}
    \vec{A} 
    &= 
    \left[
        \begin{array}{cc}
            ~\cos \omega~
            & 
            ~-\sin \omega~
            \\
            ~\sin \omega~
            &
            ~\cos \omega~
        \end{array}
    \right]
    ,
    &
    \vec{Q}
    &=
    \left[
        \begin{array}{cc}
            ~q~
            & 
            0
            \\
            0
            &
            ~q~
        \end{array}
    \right]
    .
\end{align*}
\begin{figure}[t]
    \centering
    \includegraphics[scale=0.35]{kalman-filter-ess-relabeled.pdf} 
    \\ 100 16-dimensional observations,
    \\ 500 samples after 10\,000 samples of burn-in.
\label{fig:kalman-filter}
\end{figure}
\end{frame}

\section{Summary}

\begin{frame}{Summary}
\begin{itemize}
  \item A scheme of rewarding random choices based on program
    output.
  \item An approach to propagation of choice rewards to 
    MH proposal scheduling parameters.
  \item An application of this approach to LMH, where the
    probabilities of selecting each variable for modification
    are adjusted.
\end{itemize}
\end{frame}

\begin{frame}
\begin{center}
\begin{huge}
Thank You
\end{huge}
\end{center}
\end{frame}

\end{document}
